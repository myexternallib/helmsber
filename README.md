# Запуск Jenkins в кластере minikube

Прежде всего, нужно запустить утилиту minikube, делается это командой в Терминале: 
```
minikube start
```

После этого, необходимо запустить контейнер с jenkins и helm в кластере kubernetes, можно это сделать двумя способами:  
### 1. Скопировать файлы из [репозитория](https://gitlab.com/myexternallib/jenkins-with-helm.git)  
Для этого необходимо перейти в нужную директорию в Терминале, затем ввести команду:  
```
git clone https://gitlab.com/myexternallib/jenkins-with-helm.git
```

После этого, репозиторий скопируется в директорию, необходимо перейти в нее и ввести команду:  
```
helm install demo .
```
Где demo - предпочитаемое имя чарта helm.

*Ниже приведен пример вывода при удачном выполнении команды:*

```
NAME: demo
LAST DEPLOYED: Tue Feb  6 19:55:30 2024
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

Чтобы убедиться, что кластер функционирует, необходимо ввести команду:
```
kubectl get all
```
*Ниже приведен пример вывода при удачном выполнении команды:*
```
NAME                        READY   STATUS    RESTARTS   AGE
pod/demo-67d5f96db6-j8tgw   1/1     Running   0          4s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/demo         NodePort    10.96.171.216   <none>        8080:31999/TCP   4s
service/kubernetes   ClusterIP   10.96.0.1       <none>        443/TCP          10d

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/demo   1/1     1            1           4s

NAME                              DESIRED   CURRENT   READY   AGE
replicaset.apps/demo-67d5f96db6   1         1         1       4s
```

### 2. При отсутствии возможности скопировать репозиторий с gitlab, необходимо создать манифесты чарта вручную 

1. Необходимо создать директорию, в которой будут храниться файлы helm чарта;
2. В ней необходимо создать директорию `templates`;
3. Необходимо создать в корневой директории файл `Chart.yaml`, который нужен для определения имени и версии чарта с содержимым:  
```yaml
apiVersion: v2
name: jenkins-chart
version: 0.1.0
```
4. Необходимо создать в корневой директории файл `values.yaml`, в котором записаны некоторые переменные для использования их далее с содержимым:
```yaml
persistence:
  enabled: true
  size: 1Gi
```
5. В директории `templates` необходимо создать файл `deployment.yaml`, который отвечает за управление развертыванием: количество реплик контейнеров, их имя, образ, порты, а также тома, который в них монтируются с содержимым:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app: {{ .Release.Name }}
    spec:
      containers:
        - name: {{ .Release.Name }}
          image: shedens/jenkins-with-helm
          ports:
            - containerPort: 8080
          volumeMounts:
            - name: jenkins-data
              mountPath: /var/jenkins_home
      volumes:
        - name: jenkins-data
          persistentVolumeClaim:
            claimName: {{ .Release.Name }}-pvc
```
6. В директории `templates` необходимо создать файл `jenkins-rolebinding.yaml`, который отвечает за привязку ServiceAccoint default с cluster-admin, чтоб Jenkins мог выполнять работу в кластере с содержимым:
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: default-admin-binding
subjects:
- kind: ServiceAccount
  name: default
  namespace: default
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
```
7. В директории `templates` необходимо создать файл `persistentvolumeclaim.yaml`, который отвечает за привязку тома с контейнером с содержимым:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: {{ .Release.Name }}-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: {{ .Values.persistence.size }}
```
8. В директории `templates` необходимо создать файл `persistentvolume.yaml`, который отвечает за создание тома на хосте по `hostPath` с содержимым:
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: {{ .Release.Name }}-pv
spec:
  capacity:
    storage: {{ .Values.persistence.size }}
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/data/jenkins"
```
9. В директории `templates` необходимо создать файл `service.yaml`, который отвечает за создание сервиса, с возможностью подключаться к поду извне кластера с содержимым:
```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ .Release.Name }}
spec:
  selector:
    app: {{ .Release.Name }}
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 8080
      nodePort: 31999
  type: NodePort
```

**На этом установка и запуск Jenkins в кластер minikube закончены**

# Настройка Jenkins и создание Job для разворачивания nginx

#### Обратите внимание на строку `nodePort: 31999` в файле `service.yaml`, именно по этому порту будет доступен jenkins из браузера на хосте.  
Для того, чтобы попасть в веб-интерфейс Jenkins, необходимо в Терминале ввести команду:
```
minikube ip
```
*Ниже приведен пример вывода при удачном выполнении команды:*
```
192.168.49.2
```
Соответственно, для входа в веб-интерфейс Jenkins, необходимо ввести в адресной строке браузера: 
```
192.168.49.2:31999
```
Тем самым, попадая в окно входа Jenkins(см. Рисунок 1)  
![entire Jenkins](https://i.postimg.cc/W4dYK9py/image.png)
Рисунок 1 - окно входа в Jenkins 

Для входа, необохдимо ввести команду в Терминале:
```
kubectl exec -it $(kubectl get pods --output=jsonpath='{.items[*].metadata.name}') -- cat /var/jenkins_home/secrets/initialAdminPassword
```

*Ниже приведен пример вывода при удачном выполнении команды:*
```
2e1831cf3c9b4b5bb5b12452254bb1c3
```
Необходимо ввести этот пароль в окно входа, попадая на страницу выбора плагинов (Рисунок 2)
![plugins](https://i.postimg.cc/RCYhxhTb/image.png)

Рисунок 2 - окно выбора плагинов

Необходимо выбрать секцию `Select plugins to install` и выбрать следующие плагины: `Pipeline`, `Pipeline: Stage View`, `Git`.
После успешной установки плагинов, будет предложено создать пользователя, но, в данном случае, в этом нет необходимости, поэтому этот этап пропускается, нажатием кнопки `Skip and continue as admin`, на следующей странице, необходимо проверить, что URL-адрес совпадает с тем, что в адресной строке и требуется нажать на кнопку `Save and finish`. После этого, нажатие кнопки `Start using Jenkins`, перекинет на главную страницу.  
Перед созданием задачи, рекомендуется сменить пароль, для удобства. Для этого, необходимо перейти в профиль, нажатием на `admin`,  в правом верхнем углу и из выпадающего списка выбрать `Configure` (Рисунок 3).

![conf_admin](https://i.postimg.cc/mkZTQfRq/image.png)

Рисунок 3 - конфигурация учетной записи

После этого, необходимо пролистать страницу вниз до поля `Password` и заменить пароль на удобный. После этого, нажать на кнопку `Save` внизу экрана и войти снова, с обновленным паролем.  
Далее, необходимо перейти на главную страницу Jenkins, нажатем на логотип в верхнем левом углу, затем нажать на  `Create a job`. Из предложенных вариантов выбрать Pipeline и назвать любым именем, в данном случае, это будет `nginx` (Рисунок 3).
![Pipeline](https://i.postimg.cc/SssZ8D3d/image.png)

Рисунок 4 - выбор типа задачи

Требуется пролистать вниз, до поля `Pipeline`, в нем, в подполе `Definition`, необходимо выбрать `Pipeline script from SCM`, в качестве `SCM` выбрать `Git`, в `Repository URL` указать `https://gitlab.com/myexternallib/helmsber.git`, в поле `Branch Specifier(blank for 'any')` указать */main. Внизу экрана нажать на кнопку `Save` (Рисунок 4).

![PiplineConf](https://i.postimg.cc/Gmgs6Jq8/image.png)

Рисунок 5 - конфигурация Pipeline

После чего, необходимо выйти на главную страницу, там появилась Job, справа в этой строке есть кнопка запуска, необходимо на нее нажать, чтоб Job запустилась, после чего, nginx установится в кластер (Рисунок 6).

![Start Job](https://i.postimg.cc/rFy8Mpz2/image.png)

Рисунок 6 - запуск Pipeline

# Проверка доступности nginx

Таким образом, в кластер minikube установлен nginx, посредством запуска Jenkins Pipeline, проверить его доступность и наличие можно командой:

```
kubectl get all
```

*Ниже приведен пример вывода при удачном выполнении команды:*
```
NAME                                    READY   STATUS    RESTARTS      AGE
pod/demo-67d5f96db6-j8tgw               1/1     Running   1 (73m ago)   169m
pod/nginxsber-deploy-59c8cc9979-kshjb   1/1     Running   0             16s
pod/nginxsber-deploy-59c8cc9979-n556d   1/1     Running   0             16s

NAME                               TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/demo                       NodePort    10.96.171.216   <none>        8080:31999/TCP   169m
service/kubernetes                 ClusterIP   10.96.0.1       <none>        443/TCP          10d
service/nginxsber-service-deploy   ClusterIP   10.99.147.202   <none>        32080/TCP        16s

NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/demo               1/1     1            1           169m
deployment.apps/nginxsber-deploy   2/2     2            2           16s

NAME                                          DESIRED   CURRENT   READY   AGE
replicaset.apps/demo-67d5f96db6               1         1         1       169m
replicaset.apps/nginxsber-deploy-59c8cc9979   2         2         2       16s

```

Как видно из вывода, поды nginx доступен по 32080 порту сервиса внутри кластера, проверить можно, прописав команду:
```
kubectl port-forward services/nginxsber-service-deploy 9999:32080
```
Где 9999 - порт на хосте, если он занят(если при вводе`netstat -tulpn | grep 9999` выводятся строчки), то необходимо ввести другой порт, который свободен.
*Ниже приведен пример вывода при удачном выполнении команды на проброс портов:*
```
Forwarding from 127.0.0.1:9999 -> 32080
Forwarding from [::1]:9999 -> 32080
```
Необходимо в адресной строке браузера ввести:

```
localhost:9999
```
или
```
127.0.0.1:9999
```
Таким образом, выведется сконфигурированный веб-сервер nginx с минимальным содержимым (Рисунок 7)
![Hello Sber](https://i.postimg.cc/HnD9XKcb/image.png)

Рисунок 7 - проверка nginx

Для закрытия порта, необходимо нажать `ctrl+c` в окне терминала.
### В результате имеется настроенный кластер, в котором имеется самописный образ jenkins с helm, также сконфигурированная задача Jenkins, которая загружает и разворачивает helm chart с nginx.